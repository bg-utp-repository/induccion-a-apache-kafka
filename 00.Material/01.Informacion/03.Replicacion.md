### [< Volver atrás](../../README.md)

# Replicación en Kafka

## ¿Cómo funciona la replicación?
Como vimos en la presentación, la replicación en Kafka funciona con la existencia de 1 o varios brokers que cuentan con una copia de la partición del tópico creado.

## Configuración de múltiples Kafka Brokers
Cada Kafka Broker utilizado debería tener su propio fichero de configuración específico. La configuración de los brokers será similar pero cambiando ciertas propiedades específicas en cada uno de ellos.

El fichero de configuración por defecto es : ```%KAFKA_HOME%/config/server.properties``` y ya lo hemos utilizados al momento, por ejemplo, de levantar nuestro primer broker en la siguiente sentencia:

```console
# Comienza el servicio del broker de Kafka

# Unix / Mac
$ ./bin/kafka-server-start.sh config/server.properties

# Windows
$ .\bin\windows\kafka-server-start.bat config\server.properties
```
Para la realización de este artículo se utilizarán 3 brokers.

Para ello se generarán 3 copias del fichero de configuración por defecto "server.properties" con diferente nombre (lo que facilitaría su mantenimiento):

* server-0.properties
* server-1.properties
* server-2.properties

Procederemos entonces a editar estos archivos con su editor favorito de texto (Notepad++, visual studio code, etc).

Editar las siguientes propiedades del archivo ```server-0.properties```:

```console
broker.id=0
listeners=PLAINTEXT://:9092 #-- descomentar
log.dirs=C:/kafka_2.13-2.7.0/logs/broker_0
```

Editar las siguientes propiedades del archivo ```server-1.properties```:

```console
broker.id=1
listeners=PLAINTEXT://:9093 #-- descomentar
log.dirs=C:/kafka_2.13-2.7.0/logs/broker_1
```

Editar las siguientes propiedades del archivo ```server-2.properties```:

```console
broker.id=2
listeners=PLAINTEXT://:9094 #-- descomentar
log.dirs=C:/kafka_2.13-2.7.0/logs/broker_2
```

### Entendamos las propiedades
* **broker.id=** es el identificador único del broker dentro de un cluster determinado de Kafka. El mismo debe ser único.
* **listeners=** determina el puerto en donde Kafka esperará peticiones. Al estar corriendolo dentro de una misma máquina debemos asignar puertos diferentes para cada broker.
* **log.dirs=** determina la ruta en donde Kafka guardará sus logs. Por temas de mantener una mejor organización lo hemos separado en subcarpetas (broker_x) por cada una de las instancias.

### Inicialicemos los brokers
Abra 3 consolas para levantar cada uno de los brokers de Kafka correspondientes. Recuerda: el servicio de zookeeper debe encontrarse arriba para poder iniciar los brokers de Kafka.

**Consola 1:**
```console
# Comienza el servicio del broker de Kafka 0

# Unix / Mac
$ ./bin/kafka-server-start.sh config/server-0.properties

# Windows
$ .\bin\windows\kafka-server-start.bat config\server-0.properties
```

**Consola 2:**
```console
# Comienza el servicio del broker de Kafka 1

# Unix / Mac
$ ./bin/kafka-server-start.sh config/server-1.properties

# Windows
$ .\bin\windows\kafka-server-start.bat config\server-1.properties
```

**Consola 3:**
```console
# Comienza el servicio del broker de Kafka 2

# Unix / Mac
$ ./bin/kafka-server-start.sh config/server-2.properties

# Windows
$ .\bin\windows\kafka-server-start.bat config\server-2.properties
```

Para verificar que se ha arrancado sin problemas se pueden revisar las trazas mostradas en cada uno de ellos (Por ejemplo : Verificar el id en el archivo ```meta.properties```).

## Creemos un topic para replicar
Creemos un nuevo topic con el fin de revisar que la replicación se de de manera correcta:


```console
# Comienza el servicio del broker de Kafka 2

# Unix / Mac
$ ./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 3 --partitions 2 --topic replicationtopic

# Windows
$ .\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 3 --partitions 2 --topic replicationtopic
```

## Juguemos con los comandos ya conocidos

Utilizando una nueva consola producer veamos el comportamiento de los mensajes ahora en replicación:

```console
# Unix / Mac
$ ./bin/kafka-console-producer.sh --broker-list localhost:9092 --topic replicationtopic

# Windows
$ .\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic replicationtopic
```

Insertamos un par de mensaje y mediante otra nueva consola consumer veamos los mensajes:

```console
# Unix / Mac
$ ./bin/kafka-console-consumer.sh --bootstrap-server localhost:9094 --topic replicationtopic --group grupo1

# Windows
$ .\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9094 --topic replicationtopic --group grupo1
```

Nótese que el consumidor está enviado solicitudes al broker 9094 mientras que el producir está enviando mensajes al 9092, sin embargo, los mensajes son replicados en todos los brokers que forman parte del cluster.

Todo esto tiene mucho más sentido si cada broker se encuentra en máquinas diferentes con discos duros independientes y dedicados (esto esta más relacionado con infraestructura, buenas prácticas y tuning pero hay que tenerlo en cuenta para entornos "altos").

**¡Ojalá todas las veces fuera tan fácil montar un clúster!**

### [< Volver atrás](../../README.md)