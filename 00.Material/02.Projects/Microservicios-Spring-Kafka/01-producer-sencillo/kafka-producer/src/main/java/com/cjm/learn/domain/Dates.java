package com.cjm.learn.domain;

public class Dates {

	private Integer dateId;
	private String dateDescription;
	private String dateClient;

	public Integer getDateId() {
		return dateId;
	}

	public void setDateId(Integer dateId) {
		this.dateId = dateId;
	}

	public String getDateDescription() {
		return dateDescription;
	}

	public void setDateDescription(String dateDescription) {
		this.dateDescription = dateDescription;
	}

	public String getDateClient() {
		return dateClient;
	}

	public void setDateClient(String dateClient) {
		this.dateClient = dateClient;
	}

	@Override
	public String toString() {
		return "Dates [dateId=" + dateId + ", dateDescription=" + dateDescription + ", dateClient=" + dateClient + "]";
	}

	
}
