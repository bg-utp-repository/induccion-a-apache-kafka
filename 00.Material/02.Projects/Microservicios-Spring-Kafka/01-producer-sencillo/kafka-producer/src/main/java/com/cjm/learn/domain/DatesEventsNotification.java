package com.cjm.learn.domain;

public class DatesEventsNotification {

	private Integer dateNotificationtId;

	private Dates dates;

	public DatesEventsNotification(Integer dateNotificationtId, Dates dates) {
		super();
		this.dateNotificationtId = dateNotificationtId;
		this.dates = dates;
	}

	public Integer getDateNotificationtId() {
		return dateNotificationtId;
	}

	public void setDateNotificationtId(Integer dateNotificationtId) {
		this.dateNotificationtId = dateNotificationtId;
	}

	public Dates getDates() {
		return dates;
	}

	public void setDates(Dates dates) {
		this.dates = dates;
	}

	@Override
	public String toString() {
		return "DatesEventsNotification [dateNotificationtId=" + dateNotificationtId + ", dates=" + dates + "]";
	}

}
