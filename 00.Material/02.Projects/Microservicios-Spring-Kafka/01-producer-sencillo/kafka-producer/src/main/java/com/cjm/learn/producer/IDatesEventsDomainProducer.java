package com.cjm.learn.producer;

import com.cjm.learn.domain.DatesEventsNotification;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface IDatesEventsDomainProducer {

	// Async
	void sendDatesEventNotification(DatesEventsNotification eventNotification) throws JsonProcessingException;

	

}