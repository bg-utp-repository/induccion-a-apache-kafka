package com.cjm.learn.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cjm.learn.domain.DatesEventsNotification;
import com.cjm.learn.producer.IDatesEventsDomainProducer;

@RestController
public class DatesEventsController {

	Logger log = LoggerFactory.getLogger(DatesEventsController.class);

	@Autowired
	@Qualifier("async")
	IDatesEventsDomainProducer datesEventsDomainProducer;

	@PostMapping("/events")
	public ResponseEntity<DatesEventsNotification> postDatesEventNotification(@RequestBody DatesEventsNotification datesEventNotification) throws Exception {

		// Invoke Kafka producer
		log.info("### Before invoke Kafka");

		
		// Async
		datesEventsDomainProducer.sendDatesEventNotification(datesEventNotification);

		log.info("### After invoke Kafka");

		return ResponseEntity.status(HttpStatus.CREATED).body(datesEventNotification);
	}

}
