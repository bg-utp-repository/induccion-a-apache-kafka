# :octocat: Microservicios de ejemplo con Spring y Kafka 

- En la carpeta 01 se encuentra un microservicio producer sencillo, solo descarga el codigo, importalo en eclipse o SpringToolSuite y sigue las instrucciones de abajo. Cuando lo lenvantes, ve a Postman y dispare al ruta `http://localhost:8080/v1/dates/events` y enviale esto en el body con POST: 
    ``` 
    {
		"dateNotificationtId": 1,
		"dates": {
          "dateId": 123,
		  "dateDescription": "Cita dentista",
          "dateClient": "Tu Nombre"
        }
    }
    ```
    Luego ve a la terminal y dentro de tu binario de Kafka/bin/ ejecuta esto: `./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic dates-domain-events`. Listo dispara desde Postman y veras que llegan los mensajes.


- En la carpeta 02 se encuentran 2 microservicios, 1 producer y 1 consumer, al producer le envias datos para guardar o actualizar. El consumer recibe datos y los guarda en una base de datos en memoria. 

    Solo descarga el codigo, importalo en eclipse o SpringToolSuite y sigue las instrucciones de abajo. Cuando loS lenvantes, ve a Postman y dispare al ruta `http://localhost:8080/v1/libraryevent` y enviale esto en el body con POST:

    ```
    {
	    "libraryEventId": 1,
	    "book": {
	    	"bookId": 123,
	    	"bookName": "Kafka Pro",
	    	"bookAuthor": "Tu Nombre"
	    }
    }
    ```
    Luego ve a la URL `http://localhost:8081/h2-console` y veras que tienes una tabla que se llama BOOKS y LIBRARY_EVENT, veras que tienes nuevos registros ahi. Inspecciona, estudia y juega con el codigo de estos microservicios para que continues tu aprendizaje. Esto no esta lejos de lo que se desarrolla en el mercado laboral con Kafka por lo que te servira mucho. 
    
    Para Actualizar, misma ruta con metodo PUT:
    ```
    {
	    "libraryEventId": 1,
	    "book": {
	    	"bookId": 123,
	    	"bookName": "Kafka Pro update",
	    	"bookAuthor": "Tu Nombre"
	    }
    }
    ```


## Instrucciones generales

### 1. Descarga Kafka https://kafka.apache.org/downloads
### Comandos importantes, debes ejecutar cada uno antes de iniciar los microservicios
- Levantar Zookeeper: `./zookeeper-server-start.sh ../config/zookeeper.properties`
- Levantar Broker 0: `./kafka-server-start.sh ../config/server.properties`
- Levantar Broker 1: `./kafka-server-start.sh ../config/server1.properties`
- Levantar Broker 2: `./kafka-server-start.sh ../config/server2.properties`
- Dentro de la carpeta `kafka-config-guia/` veran los archivos de propiedades server, server1, server2 y zookeeper, para que simplemente los copien y peguen dentro de la carpeta `Kafka/config` en su instalación de Kafka.

- Listar los Topics creados: `./kafka-topics.sh --zookeeper localhost:2181 --list`

- Inspeccionar un Topic específico: `./kafka-topics.sh --zookeeper localhost:2181 --describe --topic library-events` o `dates-domain-events`

- Levantar la consola para consumir eventos y ver lo que envias desde un Producer: `./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic library-events`

## Levantar los microservicios desde Eclipse o STS
> Ojo Primero levanta el producer y luego el consumer
1. Gitpull
2. eclipse --> Run as --> `Maven install`
3. eclipse --> Run as --> `Spring Boot App`

### En el archivo SetUpKafka.md puedes encontrar muchos comandos utiles y su explicación