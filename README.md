# [BG-UTP] Inducción a Apache Kafka - Programa Fomento al Talento Integral TIC

## Apache Kafka
Apache Kafka es un proyecto de intermediación de mensajes de código abierto desarrollado por LinkedIn y donado a la Apache Software Foundation escrito en Java y Scala. El proyecto tiene como objetivo proporcionar una plataforma unificada, de alto rendimiento y de baja latencia para la manipulación en tiempo real de fuentes de datos. Puede verse como una cola de mensajes, bajo el patrón publicación-suscripción, masivamente escalable concebida como un registro de transacciones distribuidas,1​ lo que la vuelve atractiva para las infraestructuras de aplicaciones empresariales.

## Índice de contenido

### [0) Preparación del ambiente](00.Material/01.Informacion/00.Ambiente.md)
Conozcamos requerimientos del equipo e instalaciones requeridas para realizar una instalación de apache kafka.

### [1) Instalación y primeros pasos en Apache Kafka](00.Material/01.Informacion/01.Instalacion.md)
Aprende a descargar el material necesario para la ejecución de Apache Kafka en tu ambiente local y realiza tus primeros pasos con Apache Kafka, inicializa el servidor.

### [2) Kafka, tópicos e información general](00.Material/01.Informacion/02.Topics.md)
Aprenderemos a crear tópicos en Kafka y nos adentraremos a revisar cómo es su comportamiento.

### [3) Replicación en Kafka](00.Material/01.Informacion/03.Replicacion.md)
Aprendamos como no perder ni un sólo mensaje. Crearemos una estructura robusta a prueba de fallos.

### [4) Grupos de consumidores](00.Material/01.Informacion/04.Grupos.md)
Cómo funcionan los grupos de consumidores y cómo son útiles para agrupar componentes que funcionan como uno.

### [5) Bonus! Utilizando Kafka con Java](00.Material/01.Informacion/05.Bonus.md)
Veamos una implementación sencilla de Kafka con JAVA.